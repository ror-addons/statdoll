
Statdoll = {}


local TIME_DELAY = 1
local timeLeft = TIME_DELAY
local SD_COUNT = 34 --46

local STAT_COUNT = 8
local PlayerLevel = GameData.Player.battleLevel
local StatCap = PlayerLevel * 25 + 50
local delayedstart = 0

Base = 0
Statdoll.window = "StatdollWnd"
Statdoll.window2 = "StatdollWnd2"
Statdoll.window3 = "StatdollWnd3"
Statdoll.window4 = "StatdollWnd4"
Statdoll.window5 = "Statdollnubs"

Statdoll.StatLabels = {}
Statdoll.StatValues = {}
Statdoll.ExtraStatValues = {}
Statdoll.StatII = {}
Statdoll.StatIcons = {}
Statdoll.ExtraStatIcons = {}
Statdoll.StatFriendly = {}
Statdoll.StatIconInfo = {}
Statdoll.CurrentValues = {}
Statdoll.ExtraCurrentValues = {}
Statdoll.BaseValues = {}
Statdoll.StatReference = {}

ShowDPSMelee = 0
ShowDPSRange = 0


function Statdoll.CreateWindow()
TextLogAddEntry("Chat", 0, L"<icon=57> Statdoll 1.5.2 Loaded")
	CreateWindow(Statdoll.window, true)
	CreateWindow(Statdoll.window2, true)
	CreateWindow(Statdoll.window3, true)
	CreateWindow(Statdoll.window4, true)
	CreateWindow(Statdoll.window5, true)
	LayoutEditor.RegisterWindow( "StatdollWnd", L"Statdoll", L"Statistics Paperdoll", false, false, true, nil )
	
	Statdoll.Initialize()
	Statdoll.SetReference()
end


function Statdoll.Initialize()


-- Init the labels	
	Statdoll.StatLabels[0] = "Strength"
	Statdoll.StatLabels[1] = "Ballistic"
	Statdoll.StatLabels[2] = "Intelligence"
	Statdoll.StatLabels[3] = "Willpower"
	Statdoll.StatLabels[4] = "Weaponskill"
	Statdoll.StatLabels[5] = "Initiative"
	Statdoll.StatLabels[6] = "Toughness"
	Statdoll.StatLabels[7] = "Wounds"
	Statdoll.StatLabels[8] = "Armor"
	Statdoll.StatLabels[9] = "Spirital"
	Statdoll.StatLabels[10] = "Elemental"
	Statdoll.StatLabels[11] = "Corporeal"
	Statdoll.StatLabels[12] = "Block"
	Statdoll.StatLabels[13] = "Parry"
	Statdoll.StatLabels[14] = "Dodge"
	Statdoll.StatLabels[15] = "Disrupt"
	-- Caster stats --
	Statdoll.StatLabels[16] = "Healing Power"
	Statdoll.StatLabels[17] = "HealCrit"
	Statdoll.StatLabels[18] = "Total Healing"	
	Statdoll.StatLabels[19] = "Magic Power"
	Statdoll.StatLabels[20] = "MagicCrit"
	Statdoll.StatLabels[21] = "Total Magic"	
	-- Range stats --
	Statdoll.StatLabels[22] = "Ranged Power"
	Statdoll.StatLabels[23] = "RangeCrit"
	Statdoll.StatLabels[24] = "RangeDps"	
	Statdoll.StatLabels[25] = "Ranged DPS"
	Statdoll.StatLabels[26] = "Ranged Speed"
	Statdoll.StatLabels[27] = "RangedTotalAttack"		
	-- Melee stats --
	Statdoll.StatLabels[28] = "Melee Power"
	Statdoll.StatLabels[29] = "MeleeCrit"
	Statdoll.StatLabels[30] = "MeleeDps"	
	Statdoll.StatLabels[31] = "Melee DPS"
	Statdoll.StatLabels[32] = "Melee Speed"
	Statdoll.StatLabels[33] = "MeleeTotalAttack"	

	
-- Init the icons	
	Statdoll.StatIcons[0] = "Stricon"
	Statdoll.StatIcons[1] = "Balicon"
	Statdoll.StatIcons[2] = "Inticon"
	Statdoll.StatIcons[3] = "Wilicon"
	Statdoll.StatIcons[4] = "Weaicon"
	Statdoll.StatIcons[5] = "Iniicon"
	Statdoll.StatIcons[6] = "Touicon"
	Statdoll.StatIcons[7] = "Wouicon"
	Statdoll.StatIcons[8] = "Armicon"
	Statdoll.StatIcons[9] = "Spiicon"
	Statdoll.StatIcons[10] = "Eleicon"
	Statdoll.StatIcons[11] = "Coricon"
	Statdoll.StatIcons[12] = "Blockicon"
	Statdoll.StatIcons[13] = "Parryicon"
	Statdoll.StatIcons[14] = "Dodgeicon"
	Statdoll.StatIcons[15] = "Disrupticon"
	-- Caster stats --
	Statdoll.StatIcons[16] = "HealingPowericon"
	Statdoll.StatIcons[17] = "HealCriticon"		
	Statdoll.StatIcons[18] = "HealTotalicon"		
	Statdoll.StatIcons[19] = "MagicPowericon"
	Statdoll.StatIcons[20] = "MagicCriticon"		
	Statdoll.StatIcons[21] = "MagicTotalicon"	
	-- Range stats --
	Statdoll.StatIcons[22] = "RangePowericon"	
	Statdoll.StatIcons[23] = "RangeCriticon"			
	Statdoll.StatIcons[24] = "RangeTotalicon"	
	Statdoll.StatIcons[25] = "RangeAttackicon"
	Statdoll.StatIcons[26] = "RangeSpeedicon"	
	Statdoll.StatIcons[27] = "RangeTotalAttackicon"	
	-- Melee stats --
	Statdoll.StatIcons[28] = "MeleePowericon"	
	Statdoll.StatIcons[29] = "MeleeCriticon"		
	Statdoll.StatIcons[30] = "MeleeDpsicon"
	Statdoll.StatIcons[31] = "MeleeAttackicon"
	Statdoll.StatIcons[32] = "MeleeSpeedicon"	
	Statdoll.StatIcons[33] = "MeleeTotalAttackicon"	


	
	Statdoll.ExtraStatIcons[0] = "StrPowericon"
	Statdoll.ExtraStatIcons[1] = "BalPowericon"
	Statdoll.ExtraStatIcons[2] = "IntPowericon"
	Statdoll.ExtraStatIcons[3] = "WilPowericon"
	Statdoll.ExtraStatIcons[4] = "Apenicon"
	Statdoll.ExtraStatIcons[5] = "ToBeCriticon"
	Statdoll.ExtraStatIcons[6] = "DmgReduxicon"
	Statdoll.ExtraStatIcons[7] = "Healthicon"
	Statdoll.ExtraStatIcons[8] = "ArmPercicon"
	Statdoll.ExtraStatIcons[9] = "SpiPercicon"
	Statdoll.ExtraStatIcons[10] = "ElePercicon"
	Statdoll.ExtraStatIcons[11] = "CorPercicon"	
	Statdoll.ExtraStatIcons[12] = "ExtraBlockicon"	
	Statdoll.ExtraStatIcons[13] = "ExtraParryicon"
	Statdoll.ExtraStatIcons[14] = "ExtraDodgeicon"	
	Statdoll.ExtraStatIcons[15] = "ExtraDisrupticon"		
	-- Caster stats --
	Statdoll.ExtraStatIcons[16] = "ExtraHealPowericon"		
	Statdoll.ExtraStatIcons[17] = "ExtraHealCriticon"		
	Statdoll.ExtraStatIcons[18] = "ExtraHealTotalicon"	
	Statdoll.ExtraStatIcons[19] = "ExtraMagicPowericon"		
	Statdoll.ExtraStatIcons[20] = "ExtraMagicCriticon"		
	Statdoll.ExtraStatIcons[21] = "ExtraMagicTotalicon"	
	-- Range stats --
	Statdoll.ExtraStatIcons[22] = "ExtraRangePowericon"		
	Statdoll.ExtraStatIcons[23] = "ExtraRangeCriticon"	
	Statdoll.ExtraStatIcons[24] = "ExtraRangeTotalicon"	
	Statdoll.ExtraStatIcons[25] = "ExtraRangeAttackicon"
	Statdoll.ExtraStatIcons[26] = "ExtraRangeSpeedicon"		
	Statdoll.ExtraStatIcons[27] = "ExtraRangeTotalAttackicon"		
	-- Melee stats --
	Statdoll.ExtraStatIcons[28] = "ExtraMeleePowericon"		
	Statdoll.ExtraStatIcons[29] = "ExtraMeleeCriticon"	
	Statdoll.ExtraStatIcons[30] = "ExtraMeleeDpsicon"	
	Statdoll.ExtraStatIcons[31] = "ExtraMeleeAttackicon"
	Statdoll.ExtraStatIcons[32] = "ExtraMeleeSpeedicon"		
	Statdoll.ExtraStatIcons[33] = "ExtraMeleeTotalAttackicon"			
	
	
-- Init the values
	Statdoll.StatValues[0] = "StatdollStrengthValue"
	Statdoll.StatValues[1] = "StatdollBallisticValue"
	Statdoll.StatValues[2] = "StatdollIntelligenceValue"
	Statdoll.StatValues[3] = "StatdollWillpowerValue"
	Statdoll.StatValues[4] = "StatdollWeaponskillValue"
	Statdoll.StatValues[5] = "StatdollInitiativeValue"
	Statdoll.StatValues[6] = "StatdollToughnessValue"
	Statdoll.StatValues[7] = "StatdollWoundsValue"
	Statdoll.StatValues[8] = "StatdollArmorValue"
	Statdoll.StatValues[9] = "StatdollSpiritValue"
	Statdoll.StatValues[10] = "StatdollElemValue"
	Statdoll.StatValues[11] = "StatdollCorpValue"
	Statdoll.StatValues[12] = "StatdollBlockValue"
	Statdoll.StatValues[13] = "StatdollParryValue"
	Statdoll.StatValues[14] = "StatdollDodgeValue"
	Statdoll.StatValues[15] = "StatdollDisruptValue"
	-- Caster stats --
	Statdoll.StatValues[16] = "StatdollHealingPowerValue"
	Statdoll.StatValues[17] = "StatdollHealCritValue"
	Statdoll.StatValues[18] = "StatdollHealTotalValue"
	Statdoll.StatValues[19] = "StatdollMagicPowerValue"
	Statdoll.StatValues[20] = "StatdollMagicCritValue"
	Statdoll.StatValues[21] = "StatdollMagicTotalValue"	
	-- Range stats --
	Statdoll.StatValues[22] = "StatdollRangePowerValue"
	Statdoll.StatValues[23] = "StatdollRangeCritValue"
	Statdoll.StatValues[24] = "StatdollRangeTotalValue"
	Statdoll.StatValues[25] = "StatdollRangeAttackValue"
	Statdoll.StatValues[26] = "StatdollRangeSpeedValue"
	Statdoll.StatValues[27] = "StatdollRangeTotalAttackValue"
	-- Melee stats --
	Statdoll.StatValues[28] = "StatdollMeleePowerValue"
	Statdoll.StatValues[29] = "StatdollMeleeCritValue"	
	Statdoll.StatValues[30] = "StatdollMeleeDpsValue"
	Statdoll.StatValues[31] = "StatdollMeleeAttackValue"
	Statdoll.StatValues[32] = "StatdollMeleeSpeedValue"
	Statdoll.StatValues[33] = "StatdollMeleeTotalAttackValue"



	
	Statdoll.ExtraStatValues[0] = "StatdollStrPowerValue"
	Statdoll.ExtraStatValues[1] = "StatdollBalPowerValue"
	Statdoll.ExtraStatValues[2] = "StatdollIntPowerValue"
	Statdoll.ExtraStatValues[3] = "StatdollWilPowerValue"
	Statdoll.ExtraStatValues[4] = "StatdollApenValue"
	Statdoll.ExtraStatValues[5] = "StatdollToBeCritValue"
	Statdoll.ExtraStatValues[6] = "StatdollDmgReduxValue"
	Statdoll.ExtraStatValues[7] = "StatdollHealthValue"
	Statdoll.ExtraStatValues[8] = "StatdollArmPercValue"
	Statdoll.ExtraStatValues[9] = "StatdollSpiPercValue"
	Statdoll.ExtraStatValues[10] = "StatdollElePercValue"	
	Statdoll.ExtraStatValues[11] = "StatdollCorPercValue"
	Statdoll.ExtraStatValues[12] = "StatdollExtraBlockValue"		
	Statdoll.ExtraStatValues[13] = "StatdollExtraParryValue"	
	Statdoll.ExtraStatValues[14] = "StatdollExtraDodgeValue"		
	Statdoll.ExtraStatValues[15] = "StatdollExtraDisruptValue"		
	-- Caster stats --
	Statdoll.ExtraStatValues[16] = "StatdollExtraHealPowerValue"
	Statdoll.ExtraStatValues[17] = "StatdollExtraHealCritValue"
	Statdoll.ExtraStatValues[18] = "StatdollExtraHealTotalValue" 
	Statdoll.ExtraStatValues[19] = "StatdollExtraMagicPowerValue" 
	Statdoll.ExtraStatValues[20] = "StatdollExtraMagicCritValue"
	Statdoll.ExtraStatValues[21] = "StatdollExtraMagicTotalValue" 
	-- Range stats --
	Statdoll.ExtraStatValues[22] = "StatdollExtraRangePowerValue"
	Statdoll.ExtraStatValues[23] = "StatdollExtraRangeCritValue"
	Statdoll.ExtraStatValues[24] = "StatdollExtraRangeTotalValue" 
	Statdoll.ExtraStatValues[25] = "StatdollExtraRangeAttackValue" 
	Statdoll.ExtraStatValues[26] = "StatdollExtraRangeSpeedValue"
	Statdoll.ExtraStatValues[27] = "StatdollExtraRangeTotalAttackValue"
	-- Melee stats --
	Statdoll.ExtraStatValues[28] = "StatdollExtraMeleePowerValue"
	Statdoll.ExtraStatValues[29] = "StatdollExtraMeleeCritValue"
	Statdoll.ExtraStatValues[30] = "StatdollExtraMeleeDpsValue" 
	Statdoll.ExtraStatValues[31] = "StatdollExtraMeleeAttackValue" 
	Statdoll.ExtraStatValues[32] = "StatdollExtraMeleeSpeedValue"
	Statdoll.ExtraStatValues[33] = "StatdollExtraMeleeTotalAttackValue"	
	
	
	
	
Statdoll.SetReference()
Statdoll.GetValues()
Statdoll.SetColors()
Statdoll.FormatDefenses()
Statdoll.WriteLabels()
end


function Statdoll.SetReference()
local offhand = (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps)*0.375
if (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0) and (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0) then DualParry = 10;DualWeild = 1.5 else DualParry = 0;DualWeild = 1 end
-- Set reference values
	Statdoll.StatReference[0] = GetBonus(GameData.BonusTypes.EBONUS_STRENGTH, GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue)
	Statdoll.StatReference[1] = GetBonus(GameData.BonusTypes.EBONUS_BALLISTICSKILL, GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue)
	Statdoll.StatReference[2] = GetBonus(GameData.BonusTypes.EBONUS_INTELLIGENCE, GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue)
	Statdoll.StatReference[3] = GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)
	Statdoll.StatReference[4] = GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)
	Statdoll.StatReference[5] = GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)
	Statdoll.StatReference[6] = GetBonus(GameData.BonusTypes.EBONUS_TOUGHNESS, GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue)	
	Statdoll.StatReference[7] = GetBonus(GameData.BonusTypes.EBONUS_WOUNDS, GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue)
	Statdoll.StatReference[8] = GameData.Player.armorValue
	Statdoll.StatReference[9] = GetBonus(GameData.BonusTypes.EBONUS_SPIRIT_RESIST, GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue)
	Statdoll.StatReference[10] = GetBonus(GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue)
	Statdoll.StatReference[11] = GetBonus(GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue)
	Statdoll.StatReference[12] = ((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*5) + (GetBonus(28,0))
	Statdoll.StatReference[13] = ((GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, 0)))
	Statdoll.StatReference[14] = ((GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, 0)))
	Statdoll.StatReference[15] = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, 0)))
	-- Caster stats --
	Statdoll.StatReference[16] = (GetBonus(GameData.BonusTypes.EBONUS_HEALING_POWER, Base))
	Statdoll.StatReference[17] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_HEALING, Base))
	Statdoll.StatReference[18] = ((Statdoll.StatReference[3]/5) + (Statdoll.StatReference[16]/5))
	Statdoll.StatReference[19] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MAGIC, Base))
	Statdoll.StatReference[20] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MAGIC, Base))
	Statdoll.StatReference[21] = ((Statdoll.StatReference[2]/5) + (Statdoll.StatReference[19]/5))
	-- Range stats --
	Statdoll.StatReference[22] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_RANGED, Base))	
	Statdoll.StatReference[23] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_RANGED, Base))
	Statdoll.StatReference[24] = ((Statdoll.StatReference[1]/5) + (Statdoll.StatReference[22]/5))	
	Statdoll.StatReference[25] = (Statdoll.StatReference[1]/10) -- set for xSpeed?
	Statdoll.StatReference[26] = (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed)
	Statdoll.StatReference[27] = (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps + Statdoll.StatReference[25])
	-- Melee stats --
	Statdoll.StatReference[28] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MELEE, Base))	
	Statdoll.StatReference[29] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MELEE, Base))+(GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, Base))
	Statdoll.StatReference[30] = ((Statdoll.StatReference[0]/5) + (Statdoll.StatReference[28]/5))
	Statdoll.StatReference[31] = (Statdoll.StatReference[0]/10) -- set for xSpeed?
	Statdoll.StatReference[32] = (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed)
	Statdoll.StatReference[33] = (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps+(Statdoll.StatReference[31]*DualWeild)) + offhand
	end
--(CharacterWindow.equipmentData[GameData.EquipSlots.Left_HAND].dps*0.375)
	
function Statdoll.GetValues()

if ShowDPSMelee == 1 then
mainweaponspeed = CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed
else
mainweaponspeed = 1
end

if ShowDPSRange == 1 then
rangeweaponspeed = CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed
else
rangeweaponspeed = 1
end



if (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps < 1) then rangeweaponspeed = 1 end

if (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0) and (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0) then DualParry = 10;DualWeild = 1.5 else DualParry = 0;DualWeild = 1 end

local offhand = (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps)*0.375

-- Set current values
	Statdoll.CurrentValues[0] = GetBonus(GameData.BonusTypes.EBONUS_STRENGTH, GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue)
	Statdoll.CurrentValues[1] = GetBonus(GameData.BonusTypes.EBONUS_BALLISTICSKILL, GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue)
	Statdoll.CurrentValues[2] = GetBonus(GameData.BonusTypes.EBONUS_INTELLIGENCE, GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue)
	Statdoll.CurrentValues[3] = GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)
	Statdoll.CurrentValues[4] = GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)
	Statdoll.CurrentValues[5] = GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)
	Statdoll.CurrentValues[6] = GetBonus(GameData.BonusTypes.EBONUS_TOUGHNESS, GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue)
	Statdoll.CurrentValues[7] = GetBonus(GameData.BonusTypes.EBONUS_WOUNDS, GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue)
	Statdoll.CurrentValues[8] = GameData.Player.armorValue
	Statdoll.CurrentValues[9] = GetBonus(GameData.BonusTypes.EBONUS_SPIRIT_RESIST, GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue)
	Statdoll.CurrentValues[10] = GetBonus(GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue)
	Statdoll.CurrentValues[11] = GetBonus(GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue)
	Statdoll.CurrentValues[12] = ((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*5) + (GetBonus(28,0))
	Statdoll.CurrentValues[13] = ((GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, 0)))
	Statdoll.CurrentValues[14] = ((GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, 0)))
	Statdoll.CurrentValues[15] = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, 0)))
	-- Caster stats --
	Statdoll.CurrentValues[16] = (GetBonus(GameData.BonusTypes.EBONUS_HEALING_POWER, Base))
	Statdoll.CurrentValues[17] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_HEALING, Base))
	Statdoll.CurrentValues[18] = ((Statdoll.CurrentValues[3]/5) + (Statdoll.CurrentValues[16]/5))
	Statdoll.CurrentValues[19] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MAGIC, Base))
	Statdoll.CurrentValues[20] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MAGIC, Base))
	Statdoll.CurrentValues[21] = ((Statdoll.CurrentValues[2]/5) + (Statdoll.CurrentValues[19]/5))
	-- Range stats --
	Statdoll.CurrentValues[22] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_RANGED, Base))	
	Statdoll.CurrentValues[23] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_RANGED, Base))
	Statdoll.CurrentValues[24] = ((Statdoll.CurrentValues[1]/5) + (Statdoll.CurrentValues[22]/5))	
	Statdoll.CurrentValues[25] = (Statdoll.CurrentValues[1]/10)
	Statdoll.CurrentValues[26] = (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed)
	Statdoll.CurrentValues[27] = (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps + Statdoll.CurrentValues[25])
	-- Melee stats --
	Statdoll.CurrentValues[28] = (GetBonus(GameData.BonusTypes.EBONUS_DAMAGE_MELEE, Base))	
	Statdoll.CurrentValues[29] = (GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MELEE, Base))+(GetBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, Base))
	Statdoll.CurrentValues[30] = ((Statdoll.CurrentValues[0]/5) + (Statdoll.CurrentValues[28]/5))
	Statdoll.CurrentValues[31] = (Statdoll.CurrentValues[0]/10)
	Statdoll.CurrentValues[32] = (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed)
	Statdoll.CurrentValues[33] = (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps + (Statdoll.CurrentValues[31]*DualWeild)) + offhand
end


function Statdoll.calcextra()
	Statdoll.ExtraCurrentValues[0] = L"+"..math.floor((Statdoll.CurrentValues[0]/5))
	Statdoll.ExtraCurrentValues[1] = L"+"..math.floor((Statdoll.CurrentValues[1]/5))
	Statdoll.ExtraCurrentValues[2] = L"+"..math.floor((Statdoll.CurrentValues[2]/5))
	Statdoll.ExtraCurrentValues[3] = L"+"..math.floor((Statdoll.CurrentValues[3]/5))
	Statdoll.ExtraCurrentValues[4] = math.floor(((Statdoll.CurrentValues[4]/(GameData.Player.level*7.5+50)*.25)*100.0))..L"%"
	Statdoll.ExtraCurrentValues[5] = math.floor((GameData.Player.level*7.5+50)/10/(Statdoll.CurrentValues[5])*100)..L"%"
	Statdoll.ExtraCurrentValues[6] = L"-"..math.floor((Statdoll.CurrentValues[6]/5))
	Statdoll.ExtraCurrentValues[7] = Statdoll.CurrentValues[7]*10
	Statdoll.ExtraCurrentValues[8] = wstring.format(L"%.01f", (Statdoll.CurrentValues[8] * 0.909) / GameData.Player.level)..L"%"
	Statdoll.ExtraCurrentValues[9] = wstring.format(L"%.01f",Statdoll.CurrentValues[9]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[10] = wstring.format(L"%.01f",Statdoll.CurrentValues[10]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[11] = wstring.format(L"%.01f",Statdoll.CurrentValues[11]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[12] = (GetBonus(28,0))..L"+"..math.floor(((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*5))..L"%"
	Statdoll.ExtraCurrentValues[13] = (GetBonus(GameData.BonusTypes.EBONUS_PARRY, 0))..L"+"..math.floor(Statdoll.CurrentValues[4] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	Statdoll.ExtraCurrentValues[14] = (GetBonus(GameData.BonusTypes.EBONUS_EVADE, 0))..L"+"..math.floor(Statdoll.CurrentValues[5] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	Statdoll.ExtraCurrentValues[15] = (GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, 0))..L"+"..math.floor(Statdoll.CurrentValues[3] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	-- Caster stats --
	Statdoll.ExtraCurrentValues[16] = L"+"..wstring.format(L"%.01f",(Statdoll.CurrentValues[16]/5))
	Statdoll.ExtraCurrentValues[17] = L"+"..Statdoll.CurrentValues[17]..L"%"
	Statdoll.ExtraCurrentValues[18] = math.floor(Statdoll.ExtraCurrentValues[3]+Statdoll.ExtraCurrentValues[16])
	Statdoll.ExtraCurrentValues[19] = L"+"..wstring.format(L"%.01f",(Statdoll.CurrentValues[19]/5))
	Statdoll.ExtraCurrentValues[20] = L"+"..Statdoll.CurrentValues[20]..L"%"
	Statdoll.ExtraCurrentValues[21] = math.floor(Statdoll.ExtraCurrentValues[2]+Statdoll.ExtraCurrentValues[19])
	-- Range stats --
	Statdoll.ExtraCurrentValues[22] = L"+"..wstring.format(L"%.01f",(Statdoll.CurrentValues[22]/5))
	Statdoll.ExtraCurrentValues[23] = L"+"..Statdoll.CurrentValues[23]..L"%"
	Statdoll.ExtraCurrentValues[24] = math.floor(Statdoll.ExtraCurrentValues[1]+Statdoll.ExtraCurrentValues[22])
	Statdoll.ExtraCurrentValues[25] = L"+"..math.floor(((Statdoll.CurrentValues[1]/10)*rangeweaponspeed))
	Statdoll.ExtraCurrentValues[26]	= wstring.format(L"%.01f",(Statdoll.CurrentValues[26]))
	Statdoll.ExtraCurrentValues[27] = math.floor((Statdoll.CurrentValues[27]*rangeweaponspeed))
	-- Melee stats --
	Statdoll.ExtraCurrentValues[28] = L"+"..wstring.format(L"%.01f",(Statdoll.CurrentValues[28]/5))	
	Statdoll.ExtraCurrentValues[29] = L"+"..Statdoll.CurrentValues[29]..L"%"	
	Statdoll.ExtraCurrentValues[30] = math.floor(Statdoll.ExtraCurrentValues[0]+Statdoll.ExtraCurrentValues[28])	
	Statdoll.ExtraCurrentValues[31] = L"+"..math.floor(((Statdoll.CurrentValues[0]/10)*mainweaponspeed)*DualWeild)
	Statdoll.ExtraCurrentValues[32]	= wstring.format(L"%.01f",(Statdoll.CurrentValues[32]))	
	Statdoll.ExtraCurrentValues[33] = math.floor((Statdoll.CurrentValues[33]*mainweaponspeed))	
end



function Statdoll.SetColors()
	for i=1, SD_COUNT do
		-- unchanged, set white
if (Statdoll.StatValues[i-1] == nil) then return end
if (Statdoll.ExtraStatValues[i-1] == nil) then return end
		
		LabelSetTextColor(Statdoll.StatValues[i-1], 254, 254, 254)
		LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 254, 254, 254)

		if Statdoll.CurrentValues[i-1] > Statdoll.StatReference[i-1] then
			-- buffed, set green
			LabelSetTextColor(Statdoll.StatValues[i-1], 0, 254, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 0, 254, 0)
		end
		
		if Statdoll.CurrentValues[i-1] < Statdoll.StatReference[i-1] then
			-- debuffed, set red
			LabelSetTextColor(Statdoll.StatValues[i-1], 254, 0, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 254, 0, 0)
		end
	end

	for i=1, STAT_COUNT do
		if Statdoll.CurrentValues[i-1] > StatCap then
			-- softcapped, calculate diminishing returns and set orange
			Statdoll.CurrentValues[i-1] = math.floor( (Statdoll.CurrentValues[i-1] - StatCap) / 2 + StatCap )
			LabelSetTextColor(Statdoll.StatValues[i-1], 254, 128, 0)
		end

		-- while we're looping over the stats, round down to nearest integer for nicer display
		Statdoll.CurrentValues[i-1] = math.floor(Statdoll.CurrentValues[i-1])
	end
Statdoll.calcextra()
end


function Statdoll.FormatDefenses()
	local percCalc = 0

	percCalc = (Statdoll.CurrentValues[8] * 0.909) / GameData.Player.level
		if percCalc > 75 then
			-- at cap, color orange
			LabelSetTextColor(Statdoll.StatValues[8], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[8], 254, 128, 0)
		end

	percCalc = Statdoll.CurrentValues[9]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[9] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[9] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[9], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[9], 254, 128, 0)
			end

	percCalc = Statdoll.CurrentValues[10]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[10] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[10] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[10], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[10], 254, 128, 0)
		end

	percCalc = Statdoll.CurrentValues[11]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[11] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[11] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[11], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[11], 254, 128, 0)
		end

	--	percCalc = ((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*20) -- + (GetBonus(GameData.BonusTypes.EBONUS_BLOCK, GameData.Player.Stats[GameData.Stats.BLOCKSKILL].baseValue))
	percCalc = ((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*5) + (GetBonus(28,0))
	Statdoll.CurrentValues[12] = math.floor(percCalc)..L"%"

	percCalc = ((GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, 0)))
	Statdoll.CurrentValues[13] = math.floor(percCalc+DualParry)..L"%"

	percCalc = ((GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, 0)))
	Statdoll.CurrentValues[14] = math.floor(percCalc)..L"%"


	percCalc = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, 0)))
	Statdoll.CurrentValues[15] = math.floor(percCalc)..L"%"

	
	
	
	
end


function Statdoll.WriteLabels()
	for i=1, SD_COUNT do
		sFrn = towstring(Statdoll.StatLabels[i-1])
		sVal = towstring(Statdoll.CurrentValues[i-1])
		xVal = towstring(Statdoll.ExtraCurrentValues[i-1])
		
		
		LabelSetText(Statdoll.StatLabels[i-1], sFrn)
		LabelSetText(Statdoll.StatValues[i-1], sVal)		
		LabelSetText(Statdoll.ExtraStatValues[i-1], xVal)
		end

-- Setting the icons
		LabelSetText(Statdoll.StatIcons[0], L"<icon=100>")
		LabelSetText(Statdoll.StatIcons[1], L"<icon=107>")
		LabelSetText(Statdoll.StatIcons[2], L"<icon=108>")
		LabelSetText(Statdoll.StatIcons[3], L"<icon=102>")
		LabelSetText(Statdoll.StatIcons[4], L"<icon=106>")
		LabelSetText(Statdoll.StatIcons[5], L"<icon=105>")
		LabelSetText(Statdoll.StatIcons[6], L"<icon=103>")
		LabelSetText(Statdoll.StatIcons[7], L"<icon=104>")		
		LabelSetText(Statdoll.StatIcons[8], L"<icon=121>")
		LabelSetText(Statdoll.StatIcons[9], L"<icon=155>")
		LabelSetText(Statdoll.StatIcons[10], L"<icon=162>")
		LabelSetText(Statdoll.StatIcons[11], L"<icon=164>")
		LabelSetText(Statdoll.StatIcons[12], L"<icon=165>")	
		LabelSetText(Statdoll.StatIcons[13], L"<icon=110>")	
		LabelSetText(Statdoll.StatIcons[14], L"<icon=111>")	
		LabelSetText(Statdoll.StatIcons[15], L"<icon=112>")	
		-- Caster stats --
		LabelSetText(Statdoll.StatIcons[16], L"<icon=160>")	
		LabelSetText(Statdoll.StatIcons[17], L"<icon=163>")
		LabelSetText(Statdoll.StatIcons[18], L"<icon=156>")
		LabelSetText(Statdoll.StatIcons[19], L"<icon=156>")
		LabelSetText(Statdoll.StatIcons[20], L"<icon=163>")
		LabelSetText(Statdoll.StatIcons[21], L"<icon=163>")
		-- Range stats --
		LabelSetText(Statdoll.StatIcons[22], L"<icon=157>")	
		LabelSetText(Statdoll.StatIcons[23], L"<icon=163>")
		LabelSetText(Statdoll.StatIcons[24], L"<icon=157>")
		LabelSetText(Statdoll.StatIcons[25], L"<icon=157>")	
		LabelSetText(Statdoll.StatIcons[26], L"<icon=111>")	
		LabelSetText(Statdoll.StatIcons[27], L"<icon=111>")	
		-- Melee stats --
		LabelSetText(Statdoll.StatIcons[28], L"<icon=158>")	
		LabelSetText(Statdoll.StatIcons[29], L"<icon=163>")
		LabelSetText(Statdoll.StatIcons[30], L"<icon=159>")		
		LabelSetText(Statdoll.StatIcons[31], L"<icon=159>")		
		LabelSetText(Statdoll.StatIcons[32], L"<icon=111>")	
		LabelSetText(Statdoll.StatIcons[33], L"<icon=111>")			
		
	
		LabelSetText(Statdoll.ExtraStatIcons[0], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[1], L"<icon=156>")	
		LabelSetText(Statdoll.ExtraStatIcons[2], L"<icon=156>")
		LabelSetText(Statdoll.ExtraStatIcons[3], L"<icon=156>")
		LabelSetText(Statdoll.ExtraStatIcons[4], L"<icon=166>")
		LabelSetText(Statdoll.ExtraStatIcons[5], L"<icon=105>")
		LabelSetText(Statdoll.ExtraStatIcons[6], L"<icon=103>")
		LabelSetText(Statdoll.ExtraStatIcons[7], L"<icon=161>")
		LabelSetText(Statdoll.ExtraStatIcons[8], L"<icon=121>")	
		LabelSetText(Statdoll.ExtraStatIcons[9], L"<icon=155>")
		LabelSetText(Statdoll.ExtraStatIcons[10], L"<icon=162>")		
		LabelSetText(Statdoll.ExtraStatIcons[11], L"<icon=164>")
		LabelSetText(Statdoll.ExtraStatIcons[12], L"<icon=165>")				
		LabelSetText(Statdoll.ExtraStatIcons[13], L"<icon=106>")
		LabelSetText(Statdoll.ExtraStatIcons[14], L"<icon=105>")
		LabelSetText(Statdoll.ExtraStatIcons[15], L"<icon=102>")
		-- Caster stats --
		LabelSetText(Statdoll.ExtraStatIcons[16], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[17], L"<icon=163>")
		LabelSetText(Statdoll.ExtraStatIcons[18], L"<icon=163>")
		LabelSetText(Statdoll.ExtraStatIcons[19], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[20], L"<icon=163>")
		LabelSetText(Statdoll.ExtraStatIcons[21], L"<icon=163>")
		-- Range stats --
		LabelSetText(Statdoll.ExtraStatIcons[22], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[23], L"<icon=163>")
		LabelSetText(Statdoll.ExtraStatIcons[24], L"<icon=163>")			
		LabelSetText(Statdoll.ExtraStatIcons[25], L"<icon=107>")
		LabelSetText(Statdoll.ExtraStatIcons[26], L"<icon=111>")
		LabelSetText(Statdoll.ExtraStatIcons[27], L"<icon=111>")			
		-- Melee stats --
		LabelSetText(Statdoll.ExtraStatIcons[28], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[29], L"<icon=163>")
		LabelSetText(Statdoll.ExtraStatIcons[30], L"<icon=163>")			
		LabelSetText(Statdoll.ExtraStatIcons[31], L"<icon=100>")		
		LabelSetText(Statdoll.ExtraStatIcons[32], L"<icon=111>")		
		LabelSetText(Statdoll.ExtraStatIcons[33], L"<icon=111>")			

		
if ShowDPSRange == 0 then		
LabelSetText(Statdoll.StatLabels[25],L"Range AA/Sec")		
else
LabelSetText(Statdoll.StatLabels[25],L"Range AA/Hit")
end	

if ShowDPSMelee == 0 then		
LabelSetText(Statdoll.StatLabels[31],L"Melee AA/Sec")		
else
LabelSetText(Statdoll.StatLabels[31],L"Melee AA/Hit")
end			
		
		
end

function Statdoll.ToggleDPSMelee()
	if ShowDPSMelee == 0 then ShowDPSMelee = 1
	else ShowDPSMelee = 0
	end
end

function Statdoll.ToggleDPSRange()
	if ShowDPSRange == 0 then ShowDPSRange = 1
	else ShowDPSRange = 0
	end
end


function Statdoll.Update()
	-- Lets poll for current stats to display

if delayedstart == 0 then Statdoll.SetReference() end

delayedstart=1
	WindowSetScale("StatdollWnd2", WindowGetScale("StatdollWnd"))
	WindowSetScale("StatdollWnd3", WindowGetScale("StatdollWnd"))
	WindowSetScale("StatdollWnd4", WindowGetScale("StatdollWnd"))
	WindowSetScale("Statdollnubs", WindowGetScale("StatdollWnd"))
	
	WindowSetAlpha("StatdollWnd2", WindowGetAlpha("StatdollWnd"))
	WindowSetAlpha("StatdollWnd3", WindowGetAlpha("StatdollWnd"))
	WindowSetAlpha("StatdollWnd4", WindowGetAlpha("StatdollWnd"))

	
	Statdoll.GetValues()
	Statdoll.SetColors()
	Statdoll.FormatDefenses()
	Statdoll.WriteLabels()
end

function Statdoll.OnNubLBU()
local nubNumber	= WindowGetId (SystemData.ActiveWindow.name)
	--TidyRollOptions.Shownub(nubNumber)
if (nubNumber == 1) then 

if WindowGetShowing("StatdollWnd2") == false then
WindowSetDimensions("StatdollWnd2", 320,83)
WindowSetShowing("StatdollWnd2", true)
else
WindowSetDimensions("StatdollWnd2", 0,0)
WindowSetShowing("StatdollWnd2", false)
end


elseif (nubNumber == 2) then 
if WindowGetShowing("StatdollWnd3") == false then
WindowSetDimensions("StatdollWnd3", 320,83)
WindowSetShowing("StatdollWnd3", true)
else
WindowSetDimensions("StatdollWnd3", 0,0)
WindowSetShowing("StatdollWnd3", false)
end


elseif (nubNumber == 3) then 
if WindowGetShowing("StatdollWnd4") == false then
WindowSetDimensions("StatdollWnd4", 320,83)
WindowSetShowing("StatdollWnd4", true)
else
WindowSetDimensions("StatdollWnd4", 0,0)
WindowSetShowing("StatdollWnd4", false)
end

end
end

